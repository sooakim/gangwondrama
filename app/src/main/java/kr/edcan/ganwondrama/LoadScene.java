package kr.edcan.ganwondrama;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import org.jsoup.nodes.Document;

public class LoadScene extends AppCompatActivity {
    private Handler hd;
    private Intent goMain;
    private SharedPreferences setting;
    private SharedPreferences.Editor editor;
    private Thread thread;
    private Document doc;

    private static int BOOT_COUNT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_scene);
        setting = getSharedPreferences("setting", MODE_PRIVATE);
        editor = setting.edit();
        if(isNetWork()) {
            hd = new Handler(Looper.getMainLooper());
            hd.postDelayed(new Runnable() {
                @Override
                public void run() {
                    BOOT_COUNT = setting.getInt("boot", 0);
                    editor.putInt("boot", ++BOOT_COUNT);
                    editor.commit();
                    Log.e("LoadScene", BOOT_COUNT + "번째 앱 시작");
                    if (BOOT_COUNT == 1) {
                        new MaterialDialog.Builder(LoadScene.this)
                                .title("도움말")
                                .content("이 앱은 네트워크연결이 반드시 필요합니다")
                                .positiveText("확인")
                                .callback(new MaterialDialog.ButtonCallback() {
                                    @Override
                                    public void onPositive(MaterialDialog dialog) {
                                        dialog.dismiss();
                                    }
                                })
                                .backgroundColorRes(R.color.met)
                                .titleColorRes(R.color.white)
                                .contentColorRes(R.color.white)
                                .positiveColorRes(R.color.white)
                                .negativeColorRes(R.color.white)
                                .show();
                    }
                    goMain = new Intent(getApplicationContext(), MainActivity.class);
                    goMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(goMain);
                    finish();
                }
            }, 1500);
        }else{
            Toast.makeText(getApplicationContext(), "네트워크 연결이 필요합니다.", Toast.LENGTH_SHORT).show();
            goMain = new Intent(getApplicationContext(), SaveViewActivity.class);
            goMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(goMain);
            finish();
        }
    }

    private Boolean isNetWork() {
        ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        boolean isMobileAvailable = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isAvailable();
        boolean isMobileConnect = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnectedOrConnecting();
        boolean isWifiAvailable = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isAvailable();
        boolean isWifiConnect = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnectedOrConnecting();

        if ((isWifiAvailable && isWifiConnect) || (isMobileAvailable && isMobileConnect)) {
            return true;
        } else {
            return false;
        }
    }
}

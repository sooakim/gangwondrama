package kr.edcan.ganwondrama;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

public class ViewActivity extends AppCompatActivity implements View.OnClickListener{
    private ArrayList<ViewData> arrayList;
    private ArrayAdapter<ViewData> adapter;
    private DataSaver saver;
    private ImageView imageView;
    private RelativeLayout noimage;
    private TextView title;
    private TextView seq_num;
    private TextView location;
    private ListView listView;
    private TextView money;
    private Intent goView;
    private Thread thread;
    private SharedPreferences setting;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view);
        setActionbar(getSupportActionBar());
        setDefault();
    }

    private void setDefault() {
        setting = getSharedPreferences("setting",MODE_PRIVATE);
        saver = new DataSaver(getApplicationContext());
        editor = setting.edit();
        if(setting.getBoolean("view_dialog_view",true)) {
            new MaterialDialog.Builder(ViewActivity.this)
                    .title("도움말")
                    .content("이곳에서는 더 많은 정보를 볼 수 있습니다.\n아래로 스크롤 해보세요!\n요금 정보가 있는 경우 맨 아래 바에서 확인하실 수 있습니다.")
                    .positiveText("확인")
                    .negativeText("다시보지 않기")
                    .callback(new MaterialDialog.ButtonCallback() {
                        @Override
                        public void onPositive(MaterialDialog dialog) {
                        }

                        @Override
                        public void onNegative(MaterialDialog dialog) {
                            editor.putBoolean("view_dialog_view", false);
                            editor.commit();
                        }
                    })
                    .backgroundColorRes(R.color.met)
                    .titleColorRes(R.color.white)
                    .contentColorRes(R.color.white)
                    .positiveColorRes(R.color.white)
                    .negativeColorRes(R.color.white)
                    .show();
        }
        listView = (ListView) findViewById(R.id.listview);
        imageView= (ImageView) findViewById(R.id.imageview);
        noimage = (RelativeLayout) findViewById(R.id.noimage);
        title = (TextView) findViewById(R.id.title);
        seq_num = (TextView) findViewById(R.id.seq_num);
        location = (TextView) findViewById(R.id.location);
        money = (TextView) findViewById(R.id.money);
        goView = getIntent();
        arrayList = new ArrayList<>();
        setData();
    }

    private void setData() {
        title.setText(goView.getStringExtra("filmsite_name"));
        seq_num.setText(goView.getStringExtra("seq_num"));
        location.setText(goView.getStringExtra("gov_num"));
        if(!goView.getStringExtra("img").equals("")){
            noimage.setVisibility(View.GONE);
            imageView.setVisibility(View.VISIBLE);
            setImageView(goView.getStringExtra("img"));
        }
        if(!goView.getStringExtra("provincialcapital").equals("")){
                arrayList.add(new ViewData("소재지 정보", goView.getStringExtra("provincialcapital")));
        }
        if(!goView.getStringExtra("contact").equals("")){
            arrayList.add(new ViewData("문의처", goView.getStringExtra("contact")));
        }
        if(!goView.getStringExtra("homepage").equals("")){
            arrayList.add(new ViewData("홈페이지",goView.getStringExtra("homepage")));
        }
        if(!goView.getStringExtra("tour_infm").equals("")){
            arrayList.add(new ViewData("여행 정보",goView.getStringExtra("tour_infm")));
        }
        if (!goView.getStringExtra("course_infm").equals("")) {
            arrayList.add(new ViewData("코스 정보",goView.getStringExtra("course_infm")));
        }
        if(!goView.getStringExtra("surroundings_attraction").equals("")){
            arrayList.add(new ViewData("주변 명소 안내",goView.getStringExtra("surroundings_attraction")));
        }
        if(!goView.getStringExtra("traffic_guide").equals("")){
            arrayList.add(new ViewData("교통 안내",goView.getStringExtra("traffic_guide")));
        }
        if(!goView.getStringExtra("carpark_usefull_guide").equals("")){
            arrayList.add(new ViewData("주차장 이용 안내",goView.getStringExtra("carpark_usefull_guide")));
        }
        if((!goView.getStringExtra("regist_de").equals(""))&&(!goView.getStringExtra("updt_de").equals(""))){
            arrayList.add(new ViewData("등록 정보","등록일 : "+goView.getStringExtra("regist_de")+"\n수정일 : "+goView.getStringExtra("updt_de")));
        }
        if((!goView.getStringExtra("usefull_charge").equals(""))){
            money.setText("이용 요금 : "+goView.getStringExtra("usefull_charge")+"\"");
        }else{
            money.setText("이용 요금 정보가 없습니다!");
        }
        setListView();
    }

    private void setListView() {
        adapter = new View_Adapter(getApplicationContext(), arrayList);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView title = (TextView) view.findViewById(R.id.title);
                switch (title.getText().toString().trim()) {
                    //항목 클릭시 분기 처리
                }
            }
        });
    }

    private void setImageView(final String uri) {
        Runnable task = new Runnable() {
            @Override
            public void run() {
                try{
                    URL url = new URL(uri.trim());
                    URLConnection conn = url.openConnection();
                    conn.connect();
                    BufferedInputStream bis = new BufferedInputStream(conn.getInputStream());
                    Bitmap bm = BitmapFactory.decodeStream(bis);
                    bis.close();
                    imageView.setImageBitmap(bm);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        thread = new Thread(task);
        thread.start();
        try{
            thread.join();
        }catch (Exception e){
            //TODO
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // 기본으로 들어가는 setting 메뉴
        getMenuInflater().inflate(R.menu.menu_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home :
                finish();
                break;
            case R.id.save :
                if(goView.getStringExtra("saveview").equals("saveview")){
                    Toast.makeText(getApplicationContext(),"여기서는 찜하기를 할 수 없습니다.",Toast.LENGTH_SHORT).show();
                }else{
                    if (!saver.checkData(goView.getStringExtra("seq_num"))) {
                        saver.SetData(
                                goView.getStringExtra("title"),
                                goView.getStringExtra("content"),
                                goView.getStringExtra("location"),
                                goView.getStringExtra("seq_num"),
                                goView.getStringExtra("img"),
                                goView.getStringExtra("gov_num"),
                                goView.getStringExtra("filmsite_name"),
                                goView.getStringExtra("provincialcapital"),
                                goView.getStringExtra("contact"),
                                goView.getStringExtra("homepage"),
                                goView.getStringExtra("tour_infm"),
                                goView.getStringExtra("usefull_charge"),
                                goView.getStringExtra("carpark_usefull_guide"),
                                goView.getStringExtra("course_infm"),
                                goView.getStringExtra("surroundings_attraction"),
                                goView.getStringExtra("traffic_guide"),
                                goView.getStringExtra("regist_de"),
                                goView.getStringExtra("updt_de")
                        );
                    } else {
                        Toast.makeText(getApplicationContext(), "이미 찜한 항목 입니다.", Toast.LENGTH_SHORT).show();
                    }
                    Toast.makeText(getApplicationContext(), "찜하였습니다.", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    public void setActionbar(ActionBar actionbar) {
        actionbar.setTitle("자세히 보기");
        actionbar.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.home : finish(); break;
        }
    }
}

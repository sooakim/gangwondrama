package kr.edcan.ganwondrama;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private Spinner spinner;
    private ListView listView;
    private List_Adapter adapter;
    private ArrayList<LocationData> arrayList;
    private ArrayAdapter<String> spinner_Adapter;
    private DataParser parser;
    private Intent goView;
    private ImageView search;
    private SharedPreferences setting;
    private SharedPreferences save;
    private SharedPreferences.Editor editor2;
    private SharedPreferences.Editor editor;

    private String selected_location;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        selected_location="고성군"; //기본값
        setDefault();
        setData();
    }

    private void setData() {
            parser = new DataParser(selected_location);
            arrayList = new ArrayList<>();
            parser.initData();
            parser.loadData(getApplicationContext(),1, 100);
            arrayList = parser.getArrayList();
            adapter = new List_Adapter(getApplicationContext(),arrayList);
            listView.setAdapter(adapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    TextView seq_num = (TextView) view.findViewById(R.id.seq_num);
                    TextView img = (TextView) view.findViewById(R.id.img);
                    TextView gov_num = (TextView) view.findViewById(R.id.gov_num);
                    TextView filmsite_name = (TextView) view.findViewById(R.id.filmsite_name);
                    TextView provincialcapital = (TextView) findViewById(R.id.provincialcapital);
                    TextView contact = (TextView) view.findViewById(R.id.contact);
                    TextView homepage = (TextView) view.findViewById(R.id.homepage);
                    TextView tour_infm = (TextView) view.findViewById(R.id.tour_infm);
                    TextView usefull_charge = (TextView) view.findViewById(R.id.usefull_charge);
                    TextView carpark_usefull_guide = (TextView) view.findViewById(R.id.carpark_usefull_guide);
                    TextView course_infm = (TextView) view.findViewById(R.id.course_infm);
                    TextView surroundings_attraction = (TextView) view.findViewById(R.id.surroundings_attraction);
                    TextView traffic_guide = (TextView) view.findViewById(R.id.traffic_guide);
                    TextView regist_de = (TextView) view.findViewById(R.id.regist_de);
                    TextView updt_de = (TextView) view.findViewById(R.id.updt_de);

                    goView = new Intent(getApplicationContext(), ViewActivity.class);
                    goView.putExtra("saveview", "");
                    goView.putExtra("seq_num", seq_num.getText().toString());
                    goView.putExtra("img", img.getText().toString());
                    goView.putExtra("gov_num", gov_num.getText().toString());
                    goView.putExtra("filmsite_name", filmsite_name.getText().toString());
                    goView.putExtra("provincialcapital", provincialcapital.getText().toString());

                    goView.putExtra("contact", contact.getText().toString());
                    goView.putExtra("homepage", homepage.getText().toString());
                    goView.putExtra("tour_infm", tour_infm.getText().toString());
                    goView.putExtra("usefull_charge", usefull_charge.getText().toString());
                    goView.putExtra("carpark_usefull_guide", carpark_usefull_guide.getText().toString());
                    goView.putExtra("course_infm", course_infm.getText().toString());
                    goView.putExtra("surroundings_attraction", surroundings_attraction.getText().toString());
                    goView.putExtra("traffic_guide", traffic_guide.getText().toString());
                    goView.putExtra("regist_de", regist_de.getText().toString());
                    goView.putExtra("updt_de", updt_de.getText().toString());
                    goView.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(goView);
                }
            });
    }

    private void setDefault() {
        save = getSharedPreferences("save",MODE_PRIVATE);
        editor2 = save.edit();
        setting = getSharedPreferences("setting",MODE_PRIVATE);
        editor = setting.edit();
        if(!(save.getInt("max",0)==0)){
            Intent go = new Intent(getApplicationContext(),SaveViewActivity.class);
            go.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(go);
        }
        if(setting.getBoolean("main_dialog_view",true)) {
            new MaterialDialog.Builder(MainActivity.this)
                    .title("도움말")
                    .content("군을 선택한뒤 옆에 있는 검색 버튼을 누르면 다른 군의 정보를 볼 수 있습니다.\n각 항목을 클릭하면 자세한 정보를 볼 수 있습니다.")
                    .positiveText("확인")
                    .negativeText("다시보지 않기")
                    .callback(new MaterialDialog.ButtonCallback() {
                        @Override
                        public void onPositive(MaterialDialog dialog) {
                        }

                        @Override
                        public void onNegative(MaterialDialog dialog) {
                            editor.putBoolean("main_dialog_view", false);
                            editor.commit();
                        }
                    })
                    .backgroundColorRes(R.color.met)
                    .titleColorRes(R.color.white)
                    .contentColorRes(R.color.white)
                    .positiveColorRes(R.color.white)
                    .negativeColorRes(R.color.white)
                    .show();
        }
        selected_location="고성군";
        spinner = (Spinner) findViewById(R.id.location);
        search = (ImageView) findViewById(R.id.main_search);
        listView = (ListView) findViewById(R.id.listview);
        setSpinner();
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setData();
            }
        });
    }

    private void setSpinner() {
        String[] locationData = new String[]{
                "고성군",
                "양양군",
                "인제군",
                "양구군",
                "철원군"
        };
        spinner_Adapter = new ArrayAdapter<>(this,R.layout.spinner_textstyle,locationData);
        spinner_Adapter.setDropDownViewResource(R.layout.spinner_dropdown);
        spinner.setAdapter(spinner_Adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selected_location = spinner.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // 기본으로 들어가는 setting 메뉴
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.reset :
                new MaterialDialog.Builder(MainActivity.this)
                        .title("초기화")
                        .content("앱 데이터를 초기화 합니다.")
                        .positiveText("확인")
                        .negativeText("취소")
                        .callback(new MaterialDialog.ButtonCallback() {
                            @Override
                            public void onPositive(MaterialDialog dialog) {
                                editor.clear();
                                editor2.clear();
                                editor.commit();
                                editor2.commit();
                                Toast.makeText(getApplicationContext(),"앱이 꺼지면 다시 실행해주세요",Toast.LENGTH_SHORT).show();
                                finish();
                            }

                            @Override
                            public void onNegative(MaterialDialog dialog) {
                                dialog.dismiss();
                            }
                        })
                        .backgroundColorRes(R.color.met)
                        .titleColorRes(R.color.white)
                        .contentColorRes(R.color.white)
                        .positiveColorRes(R.color.white)
                        .negativeColorRes(R.color.white)
                        .show();
                break;
            case R.id.save :
                Intent goSave = new Intent(getApplicationContext(),SaveViewActivity.class);
                goSave.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(goSave);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }
}

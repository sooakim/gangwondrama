package kr.edcan.ganwondrama;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by kimok_000 on 2015-11-30.
 */
public class SaveAdapter extends ArrayAdapter<SaveData> {
    // 레이아웃 XML을 읽어들이기 위한 객체
    private LayoutInflater mInflater;

    public SaveAdapter(Context context, ArrayList<SaveData> object) {
        // 상위 클래스의 초기화 과정
        // context, 0, 자료구조
        super(context, 0, object);
        mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    // 보여지는 스타일을 자신이 만든 xml로 보이기 위한 구문
    @Override
    public View getView(int position, View v, ViewGroup parent) {
        View view = null;
        // 현재 리스트의 하나의 항목에 보일 컨트롤 얻기
        if (v == null) {
            // XML 레이아웃을 직접 읽어서 리스트뷰에 넣음
            view = mInflater.inflate(R.layout.content_main, null);
        } else {
            view = v;
        }
        // 자료를 받는다.
        final SaveData data = this.getItem(position);
        if (data != null) {
            //화면 출력
            TextView title = (TextView) view.findViewById(R.id.title);
            TextView content = (TextView) view.findViewById(R.id.content);
            TextView location = (TextView) view.findViewById(R.id.location);

            TextView seq_num = (TextView) view.findViewById(R.id.seq_num);
            TextView img = (TextView) view.findViewById(R.id.img);
            TextView gov_num = (TextView) view.findViewById(R.id.gov_num);
            TextView filmsite_name = (TextView) view.findViewById(R.id.filmsite_name);
            TextView provincialcapital = (TextView) view.findViewById(R.id.provincialcapital);
            TextView contact = (TextView) view.findViewById(R.id.contact);
            TextView homepage = (TextView) view.findViewById(R.id.homepage);
            TextView tour_infm = (TextView) view.findViewById(R.id.tour_infm);
            TextView usefull_charge = (TextView) view.findViewById(R.id.usefull_charge);
            TextView carpark_usefull_guide = (TextView) view.findViewById(R.id.carpark_usefull_guide);
            TextView course_infm = (TextView) view.findViewById(R.id.course_infm);
            TextView surroundings_attraction = (TextView) view.findViewById(R.id.surroundings_attraction);
            TextView traffic_guide = (TextView) view.findViewById(R.id.traffic_guide);
            TextView regist_de = (TextView) view.findViewById(R.id.regist_de);
            TextView updt_de = (TextView) view.findViewById(R.id.updt_de);

            title.setText(data.getTitle());
            content.setText(data.getContent());
            location.setText(data.getLocation());

            filmsite_name.setText(data.getFilmsite_name());
            regist_de.setText(data.getRegist_de());
            updt_de.setText(data.getUpdt_de());
            location.setText(data.getGov_num());
            seq_num.setText(data.getSeq_num());
            img.setText(data.getImg());
            gov_num.setText(data.getGov_num());
            filmsite_name.setText(data.getFilmsite_name());
            provincialcapital.setText(data.getProvincialcapital());
            contact.setText(data.getContact());
            homepage.setText(data.getHomepage());
            tour_infm.setText(data.getTour_infm());
            usefull_charge.setText(data.getUsefull_charge());
            carpark_usefull_guide.setText(data.getUsefull_charge());
            course_infm.setText(data.getCourse_infm());
            surroundings_attraction.setText(data.getSurroundings_attraction());
            traffic_guide.setText(data.getTraffic_guide());
            regist_de.setText(data.getRegist_de());
            updt_de.setText(data.getUpdt_de());


        }
        return view;
    }
}

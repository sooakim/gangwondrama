package kr.edcan.ganwondrama;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;

/**
 * Created by kimok_000 on 2015-11-30.
 */
public class DataSaver {
    private ArrayList<SaveData> arrayList;
    private int count;

    private SharedPreferences save;
    private SharedPreferences.Editor editor;
    private Context context;

    public DataSaver(Context context) {
        this.context = context;
        arrayList = new ArrayList<>();
        save = context.getSharedPreferences("save", Context.MODE_PRIVATE);
        editor = save.edit();
    }

    public void SetData(
                        String title,
                        String content,
                        String location,
                        String seq_num,
                        String img,
                        String gov_num,
                        String filmsite_name,
                        String provincialcapitcal,
                        String contact,
                        String homepage,
                        String tour_infm,
                        String usefull_charge,
                        String carpark_usefull_guide,
                        String course_infm,
                        String surroundings_attraction,
                        String traffic_guide,
                        String regist_de,
                        String updt_de) {
        count = save.getInt("max",0);

        editor.putString("title"+count,title);
        editor.putString("content"+count,content);
        editor.putString("location"+count,location);
        editor.putString("seq_num"+count,seq_num);
        editor.putString("img"+count,img);
        editor.putString("gov_num"+count,gov_num);
        editor.putString("filmsite_name"+count,filmsite_name);
        editor.putString("provincialcaptical"+count,provincialcapitcal);
        editor.putString("contact"+count,contact);
        editor.putString("homepage"+count,homepage);
        editor.putString("tour_infm"+count,tour_infm);
        editor.putString("usefull_charge"+count,usefull_charge);
        editor.putString("carpark_usefull_guide"+count,carpark_usefull_guide);
        editor.putString("course_infm"+count,course_infm);
        editor.putString("surroundings_attraction"+count,surroundings_attraction);
        editor.putString("traffic_guide"+count,traffic_guide);
        editor.putString("regist_de"+count,regist_de);
        editor.putString("updt_de"+count,updt_de);

        count++;
        editor.putInt("max",count);
        editor.commit();
    }

    public boolean checkData(String seq_num){
        for(int i=0;i<save.getInt("max",0);i++){
            if(save.getString("seq_num"+i,null).equals(seq_num)) return true;
        }
        return false;
    }

    public void resetData(){
        editor.putInt("max",0);
        editor.commit();
    }

}

package kr.edcan.ganwondrama;

import android.content.Context;
import android.util.Log;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by kimok_000 on 2015-11-30.
 */
public class DataParser {
    private final static String URL = "http://data.gwd.go.kr/apiservice";   //DB URL
    private static String KEY ; //공공DB 접근을 위한 인증키
    //4b4a7767696b696d3836726d776549
    //61527073436b696d39305068534a6d
    //414b6466416b696d383756554f7862
    //414b6466416b696d383756554f7862
    //61527073436b696d39305068534a6d
    private final static String TYPE = "xml";   //DB출력 형식
    private static String SERVICE ;     //서비스명
    //cybergt-travel-filming_site-goseong
    //cybergt-travel-filming_site-yangyang
    //cybergt-travel-filming_site-inje
    //cybergt-travel-filming_site-yanggu
    //cybergt-travel-filming_site-cheorwon
    private String url;

    private Thread thread;

    private String code = null;
    private String message = null;
    private String lost_list_count = null;

    private String[] seq_num; //일련번호
    private String[] img; //이미지 url
    private String[] gov_num;     //시군명
    private String[] filmsite_name;   //촬영지명
    private String[] provincialcapital;   //소재지
    private String[] contact; //문의처
    private String[] homepage;    //홈페이지
    private String[] tour_infm;   //여행정보
    private String[] usefull_charge;  //이용요금
    private String[] carpark_usefull_guide;   //주차장 이용안내
    private String[] course_infm; //코스 정보
    private String[] surroundings_attraction; //주변 명소
    private String[] traffic_guide;   //교통 안내
    private String[] regist_de;   //등록일
    private String[] updt_de;  //수정일

    private Elements parse;
    private Document doc;

    private ArrayList<LocationData> arrayList;

    public DataParser(String location) {
        switch (location){
            case "" : Log.e("DataParer()","location is empty"); this.KEY=null; this.SERVICE=null; break;
            case "고성군" : this.KEY="4b4a7767696b696d3836726d776549"; this.SERVICE="cybergt-travel-filming_site-goseong"; break;
            case "양양군" : this.KEY="61527073436b696d39305068534a6d"; this.SERVICE="cybergt-travel-filming_site-yangyang"; break;
            case "인제군" : this.KEY="414b6466416b696d383756554f7862"; this.SERVICE="cybergt-travel-filming_site-inje"; break;
            case "양구군" : this.KEY="414b6466416b696d383756554f7862"; this.SERVICE="cybergt-travel-filming_site-yanggu"; break;
            case "철원군" : this.KEY ="61527073436b696d39305068534a6d"; this.SERVICE="cybergt-travel-filming_site-cheorwon"; break;
        }
    }

    public void initData() {     //어레이 초기화
        arrayList = new ArrayList<>();
    }

    public void loadData(final Context context,final int start_index, final int end_index) {                 //데이터를 가져올 링크를 생성하고 데이터를 가져와서 어레이에 넣음
        Runnable task = new Runnable() {
            @Override
            public void run() {
                url = URL + "/" + KEY + "/" + TYPE +"/"+SERVICE + "/" + start_index + "/" + end_index+"/";

                try {
                    doc = Jsoup.connect(url).timeout(5000).get();
                } catch (IOException e) {
                    //TODO 인터넷 연결을 확인하세요
                }

                message = doc.select("MESSAGE").toString();
                code = doc.select("RESULT CODE").toString();
                lost_list_count = doc.select("list_total_count").toString();

                seq_num=exParse(doc,"SEQ_NUM",seq_num);
                img=exParse(doc,"IMG",img);
                gov_num=exParse(doc,"GOV_NM",gov_num);
                filmsite_name=exParse(doc,"FILMSITE_NAME",filmsite_name);
                provincialcapital=exParse(doc,"PROVINCIALCAPITAL",provincialcapital);
                contact=exParse(doc,"CONTACT",contact);
                homepage=exParse(doc,"HOMPAGE",homepage);
                tour_infm=exParse(doc,"TOUR_INFM",tour_infm);
                usefull_charge=exParse(doc,"USEFULL_CHARGE",usefull_charge);
                carpark_usefull_guide=exParse(doc,"CARPARK_USEFULL_GUIDE",carpark_usefull_guide);
                course_infm=exParse(doc,"COURSE_INFM",course_infm);
                surroundings_attraction=exParse(doc,"SURROUNDINGS_ATTRACTION",surroundings_attraction);
                traffic_guide=exParse(doc,"TRAFFIC_GUIDE",traffic_guide);
                regist_de=exParse(doc,"REGIST_DE",regist_de);
                updt_de=exParse(doc,"UPDT_DE",updt_de);

                for (int i = 0; i < seq_num.length; i++) {
                    arrayList.add(new LocationData(
                            seq_num[i],
                            img[i],
                            gov_num[i],
                            filmsite_name[i],
                            provincialcapital[i],
                            contact[i],
                            homepage[i],
                            tour_infm[i],
                            usefull_charge[i],
                            carpark_usefull_guide[i],
                            course_infm[i],
                            surroundings_attraction[i],
                            traffic_guide[i],
                            regist_de[i],
                            updt_de[i]
                    ));

                    Log.e("TTT", "몇 번 호출 되는지 확인하시오");
                }
            }
        };
        thread = new Thread(task);
        thread.start();
        try {
            thread.join();
        } catch (Exception e) {
            //todo
        }
    }   //분실물 목록 파싱 함수

    public ArrayList<LocationData> getArrayList() {      //파싱한 데이터는 모두 이 클래스에 저장되어 있으므로 다른 곳에서 사용하려면 사용
        return arrayList;
    }   //파싱 함수에서 저장한 어레이 리스트를 반환

    private String[] exParse(Document doc, String select, String[] array) {  //가져온 데이터를 쪼개서 배열에 넣는 메소드
        parse = doc.select(select);
        array = new String[parse.size()];
        for (int i = 0; i < parse.size(); i++) {
            array[i] = parse.get(i).text().toString();
        }
        return array;
    }

    public String getCode() {        //에러 코드 가져올 때 사용
        return code;
    }
}

package kr.edcan.ganwondrama;

/**
 * Created by kimok_000 on 2015-11-30.
 */
public class LocationData {
    private String seq_num; //일련번호
    private String img; //이미지 url
    private String gov_num;     //시군명
    private String filmsite_name;   //촬영지명
    private String provincialcapital;   //소재지
    private String contact; //문의처
    private String homepage;    //홈페이지
    private String tour_infm;   //여행정보
    private String usefull_charge;  //이용요금
    private String carpark_usefull_guide;   //주차장 이용안내
    private String course_infm; //코스 정보
    private String surroundings_attraction; //주변 명소
    private String traffic_guide;   //교통 안내
    private String regist_de;   //등록일
    private String updt_de;  //수정일

    public LocationData(
            String seq_num,
            String img,
            String gov_num,
            String filmsite_name,
            String provincialcapital,
            String contact,
            String homepage,
            String tour_infm,
            String usefull_charge,
            String carpark_usefull_guide,
            String course_infm,
            String surroundings_attraction,
            String traffic_guide,
            String regist_de,
            String updt_de
    ){
        this.seq_num=seq_num;
        this.img=img;
        this.gov_num=gov_num;
        this.filmsite_name=filmsite_name;
        this.provincialcapital=provincialcapital;
        this.contact=contact;
        this.homepage=homepage;
        this.tour_infm=tour_infm;
        this.usefull_charge=usefull_charge;
        this.carpark_usefull_guide=carpark_usefull_guide;
        this.course_infm=course_infm;
        this.surroundings_attraction=surroundings_attraction;
        this.traffic_guide=traffic_guide;
        this.regist_de=regist_de;
        this.updt_de=updt_de;
    }

    public String getSeq_num() {
        return seq_num;
    }

    public String getImg() {
        return img;
    }

    public String getGov_num() {
        return gov_num;
    }

    public String getFilmsite_name() {
        return filmsite_name;
    }

    public String getProvincialcapital() {
        return provincialcapital;
    }

    public String getContact() {
        return contact;
    }

    public String getHomepage() {
        return homepage;
    }

    public String getCourse_infm() {
        return course_infm;
    }

    public String getRegist_de() {
        return regist_de;
    }

    public String getSurroundings_attraction() {
        return surroundings_attraction;
    }

    public String getTour_infm() {
        return tour_infm;
    }

    public String getTraffic_guide() {
        return traffic_guide;
    }

    public String getUpdt_de() {
        return updt_de;
    }

    public String getUsefull_charge() {
        return usefull_charge;
    }

    public String getCarpark_usefull_guide() {
        return carpark_usefull_guide;
    }
}

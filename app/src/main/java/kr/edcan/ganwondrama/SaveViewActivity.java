package kr.edcan.ganwondrama;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;

public class SaveViewActivity extends AppCompatActivity implements View.OnClickListener{
    DataSaver saver;
    ListView listview;
    ListAdapter adapter;
    ArrayList<LocationData> arrayList;
    MaterialDialog a;
    SharedPreferences save;
    SharedPreferences setting;
    SharedPreferences.Editor editor2;
    SharedPreferences.Editor editor;
    private Intent goView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_view);
        saver = new DataSaver(getApplicationContext());
        setActionbar(getSupportActionBar());
        Intent asdf = getIntent();
        setDefault();
        if(save.getInt("max",0)==0){
            Toast.makeText(getApplicationContext(), "찜한 항목이 없음", Toast.LENGTH_SHORT).show();
            finish();
        }
        setData();
    }

    private void setData() {
        a = new MaterialDialog.Builder(SaveViewActivity.this)
                .title("데이터를 로드합니다")
                .content("잠시만 기다려주세요")
                .progress(true, 0)
                .show();
        arrayList = new ArrayList<>();
        //TODO : 저장된 데이터들 불러와서 어레이에 투척하기
        for(int i=0;i<save.getInt("max",0);i++){
            arrayList.add(new LocationData(
                    save.getString("seq_num"+i,null),
                    save.getString("img"+i,null),
                    save.getString("gov_num"+i,null),
                    save.getString("filmsite_name"+i,null),
                    save.getString("provincialcaptical"+i,null),
                    save.getString("contact"+i,null),
                    save.getString("homepage"+i,null),
                    save.getString("tour_infm"+i,null),
                    save.getString("usefull_charge"+i,null),
                    save.getString("carpark_usefull_guide"+i,null),
                    save.getString("course_infm"+i,null),
                    save.getString("surroundings_attraction"+i,null),
                    save.getString("traffic_guide"+i,null),
                    save.getString("regist_de"+i,null),
                    save.getString("updt_de"+i,null)
            ));
        }
        adapter = new List_Adapter(getApplicationContext(), arrayList);
        listview.setAdapter(adapter);
        a.dismiss();
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView title = (TextView) findViewById(R.id.title);
                TextView content = (TextView) findViewById(R.id.content);
                TextView location = (TextView) findViewById(R.id.location);
                TextView seq_num = (TextView) view.findViewById(R.id.seq_num);
                TextView img = (TextView) view.findViewById(R.id.img);
                TextView gov_num = (TextView) view.findViewById(R.id.gov_num);
                TextView filmsite_name = (TextView) view.findViewById(R.id.filmsite_name);
                TextView provincialcapital = (TextView) findViewById(R.id.provincialcapital);
                TextView contact = (TextView) view.findViewById(R.id.contact);
                TextView homepage = (TextView) view.findViewById(R.id.homepage);
                TextView tour_infm = (TextView) view.findViewById(R.id.tour_infm);
                TextView usefull_charge = (TextView) view.findViewById(R.id.usefull_charge);
                TextView carpark_usefull_guide = (TextView) view.findViewById(R.id.carpark_usefull_guide);
                TextView course_infm = (TextView) view.findViewById(R.id.course_infm);
                TextView surroundings_attraction = (TextView) view.findViewById(R.id.surroundings_attraction);
                TextView traffic_guide = (TextView) view.findViewById(R.id.traffic_guide);
                TextView regist_de = (TextView) view.findViewById(R.id.regist_de);
                TextView updt_de = (TextView) view.findViewById(R.id.updt_de);

                goView = new Intent(getApplicationContext(), ViewActivity.class);
                goView.putExtra("title", title.getText().toString());
                goView.putExtra("content", content.getText().toString());
                goView.putExtra("location", location.getText().toString());
                goView.putExtra("saveview", "saveview");
                goView.putExtra("seq_num", seq_num.getText().toString());
                goView.putExtra("img", img.getText().toString());
                goView.putExtra("gov_num", gov_num.getText().toString());
                goView.putExtra("filmsite_name", filmsite_name.getText().toString());
                goView.putExtra("provincialcapital", provincialcapital.getText().toString());

                goView.putExtra("contact", contact.getText().toString());
                goView.putExtra("homepage", homepage.getText().toString());
                goView.putExtra("tour_infm", tour_infm.getText().toString());
                goView.putExtra("usefull_charge", usefull_charge.getText().toString());
                goView.putExtra("carpark_usefull_guide", carpark_usefull_guide.getText().toString());
                goView.putExtra("course_infm", course_infm.getText().toString());
                goView.putExtra("surroundings_attraction", surroundings_attraction.getText().toString());
                goView.putExtra("traffic_guide", traffic_guide.getText().toString());
                goView.putExtra("regist_de", regist_de.getText().toString());
                goView.putExtra("updt_de", updt_de.getText().toString());
                goView.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(goView);
            }
        });
    }

    public void setActionbar(ActionBar actionbar) {
        actionbar.setTitle("찜한 항목");
        actionbar.setDisplayHomeAsUpEnabled(true);
    }

    private void setDefault() {
        save = getSharedPreferences("save",MODE_PRIVATE);
        setting = getSharedPreferences("setting",MODE_PRIVATE);
        editor2 = setting.edit();
        editor = save.edit();
        if(setting.getBoolean("save_dialog_view",true)) {
            new MaterialDialog.Builder(SaveViewActivity.this)
                    .title("도움말")
                    .content("여기서는 찜한 항목을 볼 수 있습니다.\n초기화 메뉴를 누르면 삭제됩니다.")
                    .positiveText("확인")
                    .negativeText("다시보지 않기")
                    .callback(new MaterialDialog.ButtonCallback() {
                        @Override
                        public void onPositive(MaterialDialog dialog) {
                        }

                        @Override
                        public void onNegative(MaterialDialog dialog) {
                            editor2.putBoolean("save_dialog_view", false);
                            editor2.commit();
                        }
                    })
                    .backgroundColorRes(R.color.met)
                    .titleColorRes(R.color.white)
                    .contentColorRes(R.color.white)
                    .positiveColorRes(R.color.white)
                    .negativeColorRes(R.color.white)
                    .show();
        }
        goView = new Intent(getApplicationContext(),ViewActivity.class);
        listview = (ListView) findViewById(R.id.listview);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // 기본으로 들어가는 setting 메뉴
        getMenuInflater().inflate(R.menu.menu_save_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home :
                finish();
                break;
            case R.id.reset :
                new MaterialDialog.Builder(SaveViewActivity.this)
                        .title("초기화")
                        .content("찜한 항목을 초기화 합니다.")
                        .positiveText("확인")
                        .negativeText("취소")
                        .callback(new MaterialDialog.ButtonCallback() {
                            @Override
                            public void onPositive(MaterialDialog dialog) {
                                editor.putInt("max",0);
                                editor.commit();
                                Toast.makeText(getApplicationContext(),"찜한 항목이 초기화 되었습니다.",Toast.LENGTH_SHORT).show();
                                finish();
                            }

                            @Override
                            public void onNegative(MaterialDialog dialog) {
                                dialog.dismiss();
                            }
                        })
                        .backgroundColorRes(R.color.met)
                        .titleColorRes(R.color.white)
                        .contentColorRes(R.color.white)
                        .positiveColorRes(R.color.white)
                        .negativeColorRes(R.color.white)
                        .show();
                break;
            case R.id.save :
                Intent goSave = new Intent(getApplicationContext(),SaveViewActivity.class);
                goSave.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(goSave);
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.home : finish(); break;
        }
    }
}

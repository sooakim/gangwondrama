package kr.edcan.ganwondrama;

/**
 * Created by kimok_000 on 2015-11-30.
 */
public class ViewData {
    private String title;
    private String content;

    public ViewData(
            String title,
            String content
    ){
        this.title=title;
        this.content=content;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }
}
